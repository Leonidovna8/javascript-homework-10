const tabs = document.querySelectorAll('.tabs-title');
const contents = document.querySelectorAll('.tabs-content li');

tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    const activeTab = document.querySelector('.tabs-title.active');
    const activeContent = document.querySelector('.tabs-content li.active-item');

    activeTab.classList.remove('active');
    tab.classList.add('active');
    activeContent.classList.remove('active-item');

    contents.forEach(content => {
      if (content.dataset.item === tab.id) {
        content.classList.add('active-item');
        content.classList.remove('no-active-item');
      } else {
        content.classList.add('no-active-item');
        content.classList.remove('active-item');
      }
    });
  });
});

